# Criação de senhas codificadas para acesso ao Backend

## Registro do usuário de serviço para autenticação no Banco de Dados

A senha é gravada encriptada no banco de dados gerado com a classe java:

```text
org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
```

O utilitário **encriptor-1.0.jar** pode ser utilizado para gerar o Hash de senha para ser armazenado no banco de dados para autenticação posterior com o comando:

```shell
java -jar encriptor-1.0.jar
```
