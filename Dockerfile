FROM gradle:6.6.1-jdk11-hotspot as builder
WORKDIR /app
RUN git clone https://gitlab.com/JSWilProf/loja_bk.git && \
    cd loja_bk && \
    ./gradlew bootJar
    
FROM adoptopenjdk:11-jdk-hotspot
VOLUME /tmp
ENV HOSTNAME database_loja \
    PORT 1433 \
    SERVERPORT 80
COPY --from=builder /app/loja_bk/build/libs/loja_bk-1.0.0.jar /tmp/app.jar
EXPOSE 80
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/tmp/app.jar","--hostname=${HOSTNAME}","--port=${PORT}","--serverport=${SERVERPORT}"]