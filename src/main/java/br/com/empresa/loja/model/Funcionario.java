package br.com.empresa.loja.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "funcionario")
public class Funcionario {
  @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idFuncionario;
	@Size(min=5, max=150, message="O nome deve ter no mínimo 3 e no máximo 150 caracteres")
	@Column(unique = true)
	private String nome;
	@OneToOne(cascade = CascadeType.ALL)
	private Usuario conta;
	private boolean desativado;
}
