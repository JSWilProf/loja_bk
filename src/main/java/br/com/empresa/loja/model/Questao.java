package br.com.empresa.loja.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "questao")
public class Questao {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idQuestao;
	@Column(nullable = false)
	private String pergunta;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="idProduto", nullable=false)
    @JsonIgnore
	private Produto produto;
	@OneToMany(mappedBy = "pergunta", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private List<Alternativa> alternativas;
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Questao other = (Questao) obj;
		if (pergunta == null) {
			if (other.pergunta != null)
				return false;
		} else if (!pergunta.equals(other.pergunta))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pergunta == null) ? 0 : pergunta.hashCode());
		return result;
	}
	
	
}