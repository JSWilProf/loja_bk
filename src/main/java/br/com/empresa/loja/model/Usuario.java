package br.com.empresa.loja.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.empresa.loja.model.validacao.Senha;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "usuario")
public class Usuario {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idUsuario;
	@Email(message = "Email inválido")
	@Column(unique = true)
	private String email;
	@Senha(message="A senha deve ter um simbolo (# & $ %) uma Maiúscula e um Nº pelo menos")
	private String senha;
	private boolean habilitado = true;
	@OneToOne(cascade = CascadeType.ALL)
	private Autorizacao autorizacao;
//	@Transient
//	@JsonIgnore
//	private boolean editar;
	@Transient
	@JsonIgnore
	private static BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

//	public boolean isAdministrador() {
//		if(autorizacao != null) {
//			return autorizacao.getPerfil().equals("ROLE_ADMIN");
//		} else {
//			return false;
//		}
//	}

//	@Transient
//	@JsonIgnore
//	public void setAdministrador(boolean admin) {
//		var adm = autorizacao;
//		
//		if(adm == null) {
//			adm = new Autorizacao();
//			adm.setLogin(login);
//		}
//		
//		if(admin) {
//			adm.setPerfil("ROLE_ADMIN");
//		} else {
//			adm.setPerfil("ROLE_USER");
//		}
//		
//		autorizacao = adm;
//	}

	public GrantedAuthority getPerfil() {
		return autorizacao != null
			? () -> autorizacao.getPerfil()
			: null;
	}

	@Transient
	@JsonIgnore
	public void setPassword(String senha) {
		this.senha = encoder.encode(senha);
	}
}
