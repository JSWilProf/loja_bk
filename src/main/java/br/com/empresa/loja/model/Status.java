package br.com.empresa.loja.model;

public enum Status {
	AGUARDANDO("Aguardando Pag."),
	FINALIZADO("Finalizado"),
	CANCELADO("Cancelado");

	private String descricao;
	
	private Status(String descricao) {
		this.descricao = descricao;
	}
	
	public String toString() {
		return descricao;
	}
}
