package br.com.empresa.loja.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "alternativa")
public class Alternativa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idAlternativa;
    @Column(nullable = false)
    private String texto;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="idQuestao", nullable=false)
    @JsonIgnore
    private Questao pergunta;
}
