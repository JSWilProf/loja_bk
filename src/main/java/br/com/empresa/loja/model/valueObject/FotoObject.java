package br.com.empresa.loja.model.valueObject;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FotoObject {
	private Long id;
	private String base64;
	private String mimeType;
	private String tipo;
}
