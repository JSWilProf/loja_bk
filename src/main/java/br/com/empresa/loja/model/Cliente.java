package br.com.empresa.loja.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.br.CPF;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "cliente")
public class Cliente {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idCliente;
	@Size(min=5, max=150, message="O nome deve ter no mínimo 3 e no máximo 150 caracteres")
	@Column(unique = true)
	private String nome;
	@Column(nullable = false)
	@Pattern(regexp = "([0-9]{2})?(\\([0-9]{2})\\)\\s?(9[0-9]{4}|[1-9][0-9]{3})-[0-9]{4}", message = "Telefone inválido")
	private String telefone;
	@Column(nullable = false)
	@CPF(message = "CPF inválido")
	private String cpf;
	@OneToOne
	private Usuario conta;
	@OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY)
	private List<Endereco> endereco;
	@OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY)
	@JsonIgnore
	private List<Pedido> pedidos;
	private boolean desativado;
}
