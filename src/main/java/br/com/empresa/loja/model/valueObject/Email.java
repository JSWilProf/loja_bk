package br.com.empresa.loja.model.valueObject;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class Email {
	@NotBlank
	private String email;
}
