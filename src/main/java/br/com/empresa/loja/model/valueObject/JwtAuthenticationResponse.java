package br.com.empresa.loja.model.valueObject;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class JwtAuthenticationResponse {
	private String accessToken;
	private LocalDateTime validTo;
	private String tokenType = "Bearer";
	private String role;
	private long id;
	
	public JwtAuthenticationResponse(String accessToken, LocalDateTime validTo) {
        this.accessToken = accessToken;
		this.validTo = validTo;
    }
	
	public JwtAuthenticationResponse(String accessToken, LocalDateTime validTo, String role, long id) {
        this.accessToken = accessToken;
		this.validTo = validTo;
		this.role = role;
		this.id = id;
    }
}
