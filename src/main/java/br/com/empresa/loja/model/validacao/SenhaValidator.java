package br.com.empresa.loja.model.validacao;

import java.util.function.BiFunction;
import java.util.function.Predicate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SenhaValidator implements ConstraintValidator<Senha, String> {
	private BiFunction<String, Predicate<Integer>, Boolean> regra = (texto, condicao) -> texto.chars()
			.filter(c -> condicao.test(c))
			.findAny()
			.isPresent();

	/*
	 * Valida a senha utilizando as seguintes regras:
	 * 
	 * - pelo menos um caracter especial
	 * - pelo menos uma letra maiúscula
	 * - conter números
	 * - ter o tamanho mínimo de 8 caracteres
	 */

	@Override
	public boolean isValid(String senha, ConstraintValidatorContext context) {
		
		return !(senha.length() < 8) &&
				regra.apply(senha, c -> c == '#' || c == '&' || c == '$' || c == '%') &&
				regra.apply(senha, Character::isUpperCase) &&
				regra.apply(senha, Character::isDigit);
	}
}
