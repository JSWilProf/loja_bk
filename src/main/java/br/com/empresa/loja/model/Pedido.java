package br.com.empresa.loja.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "pedido")
public class Pedido {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idPedido;
	@Temporal(TemporalType.DATE)
	private Date data;
	@Enumerated
	private Status status = Status.AGUARDANDO;
	@OneToMany(mappedBy = "pedido")
	private List<ItemPedido> itens;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idCliente", nullable = false)
	@JsonIgnore
	private Cliente cliente;
	
	@JsonGetter
	public Double getTotal() {
		return itens.stream()
				.map(ItemPedido::getTotal)
				.reduce(0d, Double::sum);
	}
}
