package br.com.empresa.loja.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="autorizacao")
public class Autorizacao {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idAutorizacao; 
	@Column(length=150, unique = true)
	private String email;
	private String perfil;
}
