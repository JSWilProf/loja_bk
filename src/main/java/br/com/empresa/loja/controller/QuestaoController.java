package br.com.empresa.loja.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.empresa.loja.component.exceptions.DataException;
import br.com.empresa.loja.component.exceptions.DataNotFoundException;
import br.com.empresa.loja.component.exceptions.DataValidationException;
import br.com.empresa.loja.model.Questao;
import br.com.empresa.loja.service.QuestaoService;

@RestController
@RequestMapping("/api")
public class QuestaoController {
    @Autowired
    private QuestaoService dao;

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
	@PostMapping("/produto/{id}/questao")
	public ResponseEntity<Object> salvar(@PathVariable(value = "id") long idProduto, @RequestBody 
			@Valid Questao obj, BindingResult result) throws DataException {
		if(result.hasErrors()) {
			throw new DataValidationException("Questão com dados inválidos", result);
		} else { 
			Questao questao = dao.salvar(obj, idProduto);
			return ResponseEntity.ok(questao);
		}
	}

	@Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_CLIENT"})
	@RequestMapping(
			method = RequestMethod.GET, 
			produces = "application/json;charset=utf-8",
			path = "/produto/{id}/questoes")
	public ResponseEntity<List<Questao>> listar(
			@PathVariable(value = "id") long idProduto) throws  DataException {
		return ResponseEntity.ok(dao.listar(idProduto));
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@DeleteMapping("/questao/{id}")
	public ResponseEntity<Object> remover(@PathVariable(value = "id") long id) throws DataException {
	    if (dao.remover(id)) {
			return ResponseEntity.ok().build();
		} else {
			throw new DataNotFoundException("A Questão não foi encontrada");
		}
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_CLIENT"})
	@GetMapping("/questao/{id}")
	public ResponseEntity<Questao> localizar(@PathVariable(value = "id") long id) throws DataException {
		Optional<Questao> obj = dao.localizar(id);

		if (obj.isPresent()) {
			return ResponseEntity.ok(obj.get());
		} else {
			throw new DataNotFoundException("A Questão não foi encontrada");
		}
	}
}
