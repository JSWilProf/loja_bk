package br.com.empresa.loja.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.empresa.loja.component.exceptions.DataException;
import br.com.empresa.loja.component.exceptions.DataNotFoundException;
import br.com.empresa.loja.service.AlternativaService;

@RestController
@RequestMapping("/api")
public class AlternativaController {
	@Autowired
	private AlternativaService dao;
	
	/*
	 * A remoção do Alternativa não depende do ID da Questão pelo fato de a Alternativa
	 * conter ID com chave única, desta forma permitindo que o registro seja removido
	 * sem o impacto nos dados da Questão
	 */
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@DeleteMapping("/alternativa/{id}")
	public ResponseEntity<Object> remover(@PathVariable(value = "id") long id) throws DataException {
	    if (dao.remover(id)) {
			return ResponseEntity.ok().build();
		} else {
			throw new DataNotFoundException("A Alternativa não foi encontrada");
		}
	}
}
