package br.com.empresa.loja.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.empresa.loja.component.JwtTokenProvider;
// import br.com.empresa.loja.model.Autorizacao;
import br.com.empresa.loja.model.Cliente;
import br.com.empresa.loja.model.Funcionario;
import br.com.empresa.loja.model.Usuario;
import br.com.empresa.loja.model.valueObject.JwtAuthenticationResponse;
import br.com.empresa.loja.model.valueObject.LoginRequest;
import br.com.empresa.loja.service.ClienteService;
import br.com.empresa.loja.service.FuncionarioService;
import br.com.empresa.loja.service.UsuarioService;
// import br.com.empresa.loja.repo.AutorizacaoRepo;
// import br.com.empresa.loja.repo.FuncionarioRepo;
// import br.com.empresa.loja.repo.UsuarioRepo;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private ClienteService clienteService;
	@Autowired
	private FuncionarioService funcionarioService;
	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private JwtTokenProvider tokenProvider;

	private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

	@PostMapping(value = "/login")
	public ResponseEntity<JwtAuthenticationResponse> authenticateClient(@Valid @RequestBody LoginRequest loginRequest) {
		try {
			Authentication authentication = null;
			//logger.info("Login Info > E-mail: " + loginRequest.getEmail() + " Senha: " + loginRequest.getSenha());
			try {
				authentication = authenticationManager.authenticate(
						new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getSenha()));
	
				SecurityContextHolder.getContext().setAuthentication(authentication);
			} catch (AuthenticationException ex) {
				logger.info("Erro: " + ex.getMessage());
			}

			Long id = null;
			Optional<Usuario> oUsuario = usuarioService.localizar(loginRequest.getEmail());
			if(oUsuario.isPresent()) {
				Optional<Cliente> oCliente = clienteService.localizar(oUsuario.get());
				if(oCliente.isPresent()) {
					id = oCliente.get().getIdCliente();
				} else {
					Optional<Funcionario> oFuncionario = funcionarioService.localizar(oUsuario.get());
					if(oFuncionario.isPresent()) {
						id = oFuncionario.get().getIdFuncionario();
					}
				}
			}

			return id == null
				? ResponseEntity.ok(tokenProvider.generateToken(authentication))
				: ResponseEntity.ok(tokenProvider.generateToken(authentication, id, oUsuario.get().getAutorizacao().getPerfil()));
	
		} catch (Exception ex) {
			return ResponseEntity.badRequest().build();
		}
	}

	// @Autowired
	// private FuncionarioRepo funcionarioRepo;
	// @Autowired
	// private AutorizacaoRepo authRepo;
	// @Autowired
	// private UsuarioRepo userRepo;

	// @PostMapping(value = "/initialize")
	// public ResponseEntity<Object> init(@Valid @RequestBody LoginRequest loginRequest) {
	// 	logger.info("Initializing...");
	// 	if(loginRequest.getEmail().equals("admin@domain.com") &&
	// 		loginRequest.getSenha().equals("Xzklpk9$tn0")) {
	// 		logger.info("Authorizing...");
	// 		Autorizacao auth = new Autorizacao();
	// 		auth.setEmail("adminX@domain.com");
	// 		auth.setPerfil("ROLE_ADMIN");
	// 		Usuario user = new Usuario();
	// 		user.setAutorizacao(auth);
	// 		user.setEmail("adminX@domain.com");
	// 		user.setHabilitado(true);
	// 		user.setPassword("adminX");
	// 		Funcionario func = new Funcionario();
	// 		func.setConta(user);
	// 		func.setDesativado(false);
	// 		func.setNome("adminX");
	// 		try {
	// 			authRepo.save(auth);
	// 			userRepo.save(user);
	// 			funcionarioRepo.save(func);
	// 			logger.info("Done.");
	// 			return ResponseEntity.ok().build();
	// 		}catch(Exception ex) {
	// 			logger.info("Fault.");
	// 			return ResponseEntity.unprocessableEntity().build();
	// 		}
	// 	}
	// 	logger.info("None.");
	// 	return ResponseEntity.notFound().build();
	// }
}
