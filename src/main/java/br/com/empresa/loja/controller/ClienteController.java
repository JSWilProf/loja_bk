package br.com.empresa.loja.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.empresa.loja.component.exceptions.DataException;
import br.com.empresa.loja.component.exceptions.DataNotFoundException;
import br.com.empresa.loja.component.exceptions.DataValidationException;
import br.com.empresa.loja.model.Cliente;
import br.com.empresa.loja.service.ClienteService;

@RestController
@RequestMapping("/api")
public class ClienteController {
	@Autowired
	private ClienteService dao;
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@PostMapping("/cliente")
	public ResponseEntity<Object> salvar(@RequestBody 
			@Valid Cliente obj, BindingResult result) throws DataException {
		if(result.hasErrors()) {
			throw new DataValidationException("Cliente com dados inválidos", result);
		} else {
			Cliente cliente = dao.salvar(obj);
			return ResponseEntity.ok(cliente);
		}
	}

	@RequestMapping(
			method = RequestMethod.GET, 
			produces = "application/json;charset=utf-8",
			path = "/clientes")
	public ResponseEntity<Page<Cliente>> listar(
			@RequestParam(defaultValue = "") String nome,
			@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize,
			@RequestParam(defaultValue = "idCliente") String sortBy) {
		return ResponseEntity.ok(dao.listar(nome, pageNo, pageSize, sortBy));
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@DeleteMapping("/cliente/{id}")
	public ResponseEntity<Object> remover(@PathVariable(value = "id") long id) throws DataException {
	    if (dao.remover(id)) {
			return ResponseEntity.ok().build();
		} else {
			throw new DataNotFoundException("O Cliente não foi encontrado");
		}
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@GetMapping("/cliente/{id}")
	public ResponseEntity<Cliente> localizar(@PathVariable(value = "id") long id) throws DataException {
		Optional<Cliente> obj = dao.localizar(id);

		if (obj.isPresent()) {
			return ResponseEntity.ok(obj.get());
		} else {
			throw new DataNotFoundException("O Cliente não foi encontrado");
		}	}
}
