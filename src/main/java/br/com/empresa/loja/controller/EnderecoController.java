package br.com.empresa.loja.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.empresa.loja.component.exceptions.DataException;
import br.com.empresa.loja.component.exceptions.DataNotFoundException;
import br.com.empresa.loja.component.exceptions.DataValidationException;
import br.com.empresa.loja.model.Endereco;
import br.com.empresa.loja.service.EnderecoService;

@RestController
@RequestMapping("/api")
public class EnderecoController {
	@Autowired
	private EnderecoService dao;
	
	@Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_CLIENT"})
	@PostMapping("/cliente/{id}/endereco")
	public ResponseEntity<Object> salvar(@PathVariable(value = "id") long idCliente, @RequestBody 
			@Valid Endereco obj, BindingResult result) throws DataException {
		if(result.hasErrors()) {
			throw new DataValidationException("Endereço com dados inválidos", result);
		} else {
			Endereco endereco = dao.salvar(obj, idCliente);
			return ResponseEntity.ok(endereco);
		}
	}

	@Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_CLIENT"})
	@RequestMapping(
			method = RequestMethod.GET, 
			produces = "application/json;charset=utf-8",
			path = "/cliente/{id}/enderecos")
	public ResponseEntity<List<Endereco>> listar(
			@PathVariable(value = "id") long idCliente) throws  DataException {
		return ResponseEntity.ok(dao.listar(idCliente));
	}
	
	/*
	 * A remoção do Endereço não depende do ID do Cliente pelo fato de o Endereço
	 * conter ID com chave única, desta forma permitindo que o registro seja removido
	 * sem o impacto nos dados do Cliente
	 */
	@Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_CLIENT"})
	@DeleteMapping("/endereco/{id}")
	public ResponseEntity<Object> remover(@PathVariable(value = "id") long id) throws DataException {
	    if (dao.remover(id)) {
			return ResponseEntity.ok().build();
		} else {
			throw new DataNotFoundException("O Endereço não foi encontrado");
		}
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_CLIENT"})
	@GetMapping("/endereco/{id}")
	public ResponseEntity<Endereco> localizar(@PathVariable(value = "id") long id) throws DataException {
		Optional<Endereco> obj = dao.localizar(id);

		if (obj.isPresent()) {
			return ResponseEntity.ok(obj.get());
		} else {
			throw new DataNotFoundException("O Endereço não foi encontrado");
		}
	}
}
