package br.com.empresa.loja.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.empresa.loja.component.exceptions.DataException;
import br.com.empresa.loja.component.exceptions.DataNotFoundException;
import br.com.empresa.loja.component.exceptions.DataValidationException;
import br.com.empresa.loja.model.Funcionario;
import br.com.empresa.loja.service.FuncionarioService;

@RestController
@RequestMapping("/api")
public class FuncionarioController {
	@Autowired
	private FuncionarioService dao;
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@PostMapping("/funcionario")
	public ResponseEntity<Object> salvar(@RequestBody 
			@Valid Funcionario obj, BindingResult result) throws DataException {
		if(result.hasErrors()) {
			throw new DataValidationException("Funcionário com dados inválidos", result);
		} else {
			Funcionario Funcionario = dao.salvar(obj);
			return ResponseEntity.ok(Funcionario);
		}
	}

	@RequestMapping(
			method = RequestMethod.GET, 
			produces = "application/json;charset=utf-8",
			path = "/funcionarios")
	public ResponseEntity<Page<Funcionario>> listar(
			@RequestParam(defaultValue = "") String nome,
			@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize,
			@RequestParam(defaultValue = "idFuncionario") String sortBy) {
		return ResponseEntity.ok(dao.listar(nome, pageNo, pageSize, sortBy));
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@DeleteMapping("/funcionario/{id}")
	public ResponseEntity<Object> remover(@PathVariable(value = "id") long id) throws DataException {
	    if (dao.remover(id)) {
			return ResponseEntity.ok().build();
		} else {
			throw new DataNotFoundException("O Funcionário não foi encontrado");
		}
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@GetMapping("/funcionario/{id}")
	public ResponseEntity<Funcionario> localizar(@PathVariable(value = "id") long id) throws DataException {
		Optional<Funcionario> obj = dao.localizar(id);

		if (obj.isPresent()) {
			return ResponseEntity.ok(obj.get());
		} else {
			throw new DataNotFoundException("O Funcionário não foi encontrado");
		}
	}
}
