package br.com.empresa.loja.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.empresa.loja.component.exceptions.DataException;
import br.com.empresa.loja.component.exceptions.DataNotFoundException;
import br.com.empresa.loja.component.exceptions.DataValidationException;
import br.com.empresa.loja.model.Pedido;
import br.com.empresa.loja.service.PedidoService;

@RestController
@RequestMapping("/api")
public class PedidoController {
    @Autowired
    private PedidoService dao;

    @Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_CLIENT"})
	@PostMapping("/cliente/{id}/pedido")
	public ResponseEntity<Object> salvar(@PathVariable(value = "id") long idCliente, @RequestBody 
			@Valid Pedido obj, BindingResult result) throws DataException {
		if(result.hasErrors()) {
			throw new DataValidationException("Pedido com dados inválidos", result);
		} else { 
			Pedido pedido = dao.salvar(obj, idCliente);
			return ResponseEntity.ok(pedido);
		}
	}

	@Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_CLIENT"})
	@RequestMapping(
			method = RequestMethod.GET, 
			produces = "application/json;charset=utf-8",
			path = "/cliente/{id}/pedidos")
	public ResponseEntity<List<Pedido>> listar(
			@PathVariable(value = "id") long idCliente) throws  DataException {
		return ResponseEntity.ok(dao.listar(idCliente));
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_CLIENT"})
	@PatchMapping("/pedido/{id}")
	public ResponseEntity<Object> cancelar(@PathVariable(value = "id") long id) throws DataException {
	    if (dao.cancelar(id)) {
			return ResponseEntity.ok().build();
		} else {
			throw new DataNotFoundException("O Pedido não foi encontrado");
		}
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_CLIENT"})
	@GetMapping("/pedido/{id}")
	public ResponseEntity<Pedido> localizar(@PathVariable(value = "id") long id) throws DataException {
		Optional<Pedido> obj = dao.localizar(id);

		if (obj.isPresent()) {
			return ResponseEntity.ok(obj.get());
		} else {
			throw new DataNotFoundException("O Pedido não foi encontrado");
		}
	}
}
