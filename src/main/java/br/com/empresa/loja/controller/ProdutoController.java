package br.com.empresa.loja.controller;

import java.io.IOException;
import java.util.Optional;

import javax.validation.Valid;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.empresa.loja.component.exceptions.DataException;
import br.com.empresa.loja.component.exceptions.DataNotFoundException;
import br.com.empresa.loja.component.exceptions.DataValidationException;
import br.com.empresa.loja.model.Produto;
import br.com.empresa.loja.model.valueObject.FotoObject;
import br.com.empresa.loja.service.ProdutoService;

@RestController
@RequestMapping("/api")
public class ProdutoController {
	@Autowired
	private ProdutoService dao;
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@PostMapping("/produto")
	public ResponseEntity<Object> salvar(@RequestBody 
			@Valid Produto obj, BindingResult result) throws DataException {
		if(result.hasErrors()) {
			throw new DataValidationException("Produto com dados inválidos", result);
		} else {
			Produto Produto = dao.salvar(obj);
			return ResponseEntity.ok(Produto);
		}
	}

	@RequestMapping(
			method = RequestMethod.GET, 
			produces = "application/json;charset=utf-8",
			path = "/produtos")
	public ResponseEntity<Page<Produto>> listar(
			@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize,
			@RequestParam(defaultValue = "idProduto") String sortBy) {
		return ResponseEntity.ok(dao.listar(pageNo, pageSize, sortBy));
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@DeleteMapping("/produto/{id}")
	public ResponseEntity<Object> remover(@PathVariable(value = "id") long id) throws DataException {
	    if (dao.remover(id)) {
			return ResponseEntity.ok().build();
		} else {
			throw new DataNotFoundException("O Produto não foi encontrado");
		}
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_CLIENT"})
	@GetMapping("/produto/{id}")
	public ResponseEntity<Produto> localizar(@PathVariable(value = "id") long id) throws DataException {
		Optional<Produto> obj = dao.localizar(id);

		if (obj.isPresent()) {
			return ResponseEntity.ok(obj.get());
		} else {
			throw new DataNotFoundException("O Produto não foi encontrado");
		}
	}

	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@PostMapping("/produto/{id}/foto")
	public ResponseEntity<Object> carregaFoto(@RequestParam("foto") MultipartFile foto, 
			@PathVariable(value = "id") long id) throws DataException {
	    if (foto.getSize() <= 10485760) { // 10Mb
	    	Optional<Produto> obj = dao.localizar(id);
	    	
			if (obj.isPresent()) {
				Produto Produto = obj.get();
				try {
					Produto.setFoto(foto.getBytes());
					Produto.setMimeType(foto.getContentType());
					dao.salvarDados(Produto);
					return ResponseEntity.ok(obj);
				} catch(IOException ex) {
					return ResponseEntity.unprocessableEntity().build();
				}
			} else {
				return ResponseEntity.notFound().build();
			}
		} else {
			return ResponseEntity.badRequest().build();
		}
	}

	@Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_CLIENT"})
	@GetMapping("/produto/{id}/foto")
	public ResponseEntity<Object> leFoto(@PathVariable(value = "id") long id) {
		Optional<Produto> obj = dao.localizar(id);

		if (obj.isPresent()) {
			Produto curso = obj.get();
			return ResponseEntity.ok(FotoObject.builder()
					.id(curso.getIdProduto())
					.base64(DatatypeConverter.printBase64Binary(curso.getFoto()))
					.mimeType(curso.getMimeType())
					.build());
		} else {
			return ResponseEntity.notFound().build();
		}
	}
}
