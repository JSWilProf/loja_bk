package br.com.empresa.loja.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import br.com.empresa.loja.model.Usuario;
import br.com.empresa.loja.repo.UsuarioRepo;

@Service
public class UsuarioService {
	@Autowired
	private UsuarioRepo user;
	
	// Cria uma implementação anônima da interface
	// GrantedAuthority cujo método getAuthority()
	// retorna a autorização do usuário localizado
	public GrantedAuthority getAutorizacoes(String login) {
		var oUsuario = user.findByEmail(login);
		return oUsuario.map(Usuario::getPerfil).orElse(null);
	}
	
	public Optional<Usuario> localizar(String email) {
		return user.findByEmail(email);
	}
}
