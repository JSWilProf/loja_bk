package br.com.empresa.loja.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import br.com.empresa.loja.component.exceptions.DataConflictException;
import br.com.empresa.loja.component.exceptions.DataException;
import br.com.empresa.loja.component.exceptions.DataNotFoundException;
import br.com.empresa.loja.model.Produto;
import br.com.empresa.loja.model.Questao;
import br.com.empresa.loja.repo.AlternativaRepo;
import br.com.empresa.loja.repo.QuestaoRepo;

@Service
public class QuestaoService {
    @Autowired
    private QuestaoRepo questaoRepo;
	@Autowired
	private AlternativaRepo alternativaRepo;
    @Autowired
    private ProdutoService produtoService;

    public List<Questao> listar(long idProduto) throws DataException {
        Optional<Produto> produto = produtoService.localizar(idProduto);
		if(!produto.isPresent()) {
			throw new DataNotFoundException("Questão não encontrada");
		} else {
			return questaoRepo.findByProduto(produto.get());
		}
    }

    /*
     * Para simplificar a alteração das Alternativas de uma Questão as
     * Alternativas já cadastradas para uma Questão serão removidas e
     * a lista de Alternativas que são recebidas com o objeto Questão
     * oriundo da requisição de atualização serão cadastradas.
     */
    public Questao salvar(Questao obj, long idProduto) throws DataException {
        if(obj.getIdQuestao() != null) {
			Questao questao = localizar(obj.getIdQuestao()).orElse(null);
			if(questao != null) {
				questao.getAlternativas().stream()
					.map(alternativa -> alternativa.getIdAlternativa())
					.collect(Collectors.toList())
					.forEach(idAlternativa -> alternativaRepo.deleteById(idAlternativa));

                questao.setPergunta(obj.getPergunta());
                questao.setAlternativas(obj.getAlternativas().stream()
	        		.map(alternativa -> {
	        			alternativa.setPergunta(questao);
	        			return alternativaRepo.save(alternativa);
	        		}).collect(Collectors.toList()));

				return questaoRepo.save(questao);
			} else {
				throw new DataNotFoundException("A Questão não foi encontrada");
			}
		} else {
			try {
				Optional<Produto> oProduto = produtoService.localizar(idProduto);
				if(oProduto.isPresent()) {
					Produto produto = oProduto.get();
					obj.setProduto(produto);
					
					List<Questao> lista = produto.getQuestoes();
					if(lista == null) lista = new ArrayList<>();
					if(!lista.contains(obj)) {
						Questao questao =  questaoRepo.save(obj);
						questao.setAlternativas(obj.getAlternativas().stream()
							.map(alternativa -> {
			        			alternativa.setPergunta(questao);
			        			return alternativaRepo.save(alternativa);
			        		}).collect(Collectors.toList()));
						lista.add(questao);
						produto.setQuestoes(lista);
						produtoService.salvar(produto);

						return questao;
					} else {
						throw new DataConflictException("Questão duplicada");
					}
				} else {
					throw new DataNotFoundException("Produto não encontrado");
				}
			} catch(DataIntegrityViolationException ex) {
				throw new DataConflictException("Questão duplicada");
			}
		}
    }

    public boolean remover(long id) throws DataException {
        Optional<Questao> obj = questaoRepo.findById(id);
        if(obj.isPresent()) {
            Questao questao = obj.get();
            questaoRepo.delete(questao);
            return true;
        }
        return false;
    }

    public Optional<Questao> localizar(long id) {
        return questaoRepo.findById(id);
    }
}
