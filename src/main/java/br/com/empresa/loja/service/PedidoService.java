package br.com.empresa.loja.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import br.com.empresa.loja.component.exceptions.DataConflictException;
import br.com.empresa.loja.component.exceptions.DataException;
import br.com.empresa.loja.component.exceptions.DataNotFoundException;
import br.com.empresa.loja.model.Cliente;
import br.com.empresa.loja.model.ItemPedido;
import br.com.empresa.loja.model.Pedido;
import br.com.empresa.loja.model.Status;
import br.com.empresa.loja.repo.ClienteRepo;
import br.com.empresa.loja.repo.ItemPedidoRepo;
import br.com.empresa.loja.repo.PedidoRepo;
import br.com.empresa.loja.repo.RespostaRepo;

@Service
public class PedidoService {
    @Autowired
    private PedidoRepo pedidoRepo;
    @Autowired
    private ItemPedidoRepo itemRepo;
    @Autowired
    private RespostaRepo respostaRepo;
    @Autowired
    private ClienteRepo clienteRepo;

    public List<Pedido> listar(long idCliente) throws DataException {
        Optional<Cliente> cliente = clienteRepo.findById(idCliente);
		if(!cliente.isPresent()) {
			throw new DataNotFoundException("Pedido não encontrado");
		} else {
			return pedidoRepo.findByCliente(cliente.get());
		}
    }

    public Pedido salvar(Pedido obj, long idCliente) throws DataException {
        if(obj.getIdPedido() != null) {
			Pedido pedido = localizar(obj.getIdPedido()).orElse(null);
			if(pedido != null) {
				pedido.getItens().stream()
					.map(item -> item.getIdItem())
					.collect(Collectors.toList())
					.forEach(idItem -> itemRepo.deleteById(idItem));

				pedido.setItens(obj.getItens().stream()
	        		.map(item -> {
	        			item.setPedido(pedido);
	        			return itemRepo.save(item);
	        		}).collect(Collectors.toList()));

				return pedidoRepo.save(pedido);
			} else {
				throw new DataNotFoundException("O Pedido não foi encontrado");
			}
		} else {
			try {
				Optional<Cliente> oCliente = clienteRepo.findById(idCliente);
				if(oCliente.isPresent()) {
					Cliente cliente = oCliente.get();
					obj.setCliente(cliente);
					
					List<Pedido> lista = cliente.getPedidos();
					if(lista == null) lista = new ArrayList<>();
					if(!lista.contains(obj)) {
						Pedido pedido =  pedidoRepo.save(obj);
						pedido.setItens(obj.getItens().stream()
							.map(item -> {
			        			item.setPedido(pedido);
			        			ItemPedido novoItem = itemRepo.save(item);
			        			novoItem.setRespostas(item.getRespostas().stream()
									.map(resposta -> {
										resposta.setItemPedido(item);
										return respostaRepo.save(resposta);
									}).collect(Collectors.toList()));
			        			return novoItem;
			        		}).collect(Collectors.toList()));
						lista.add(pedido);
						cliente.setPedidos(lista);
						clienteRepo.save(cliente);

						return pedido;
					} else {
						throw new DataConflictException("Pedido duplicado");
					}
				} else {
					throw new DataNotFoundException("Cliente não encontrado");
				}
			} catch(DataIntegrityViolationException ex) {
				throw new DataConflictException("Pedido duplicado");
			}
		}
    }

    public boolean cancelar(long id) throws DataException {
        Optional<Pedido> obj = pedidoRepo.findById(id);
        if(obj.isPresent()) {
            Pedido pedido = obj.get();
            pedido.setStatus(Status.CANCELADO);
            pedidoRepo.save(pedido);
            return true;
        }
        return false;
    }

    public Optional<Pedido> localizar(long id) {
        return pedidoRepo.findById(id);
    }
}
