package br.com.empresa.loja.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.empresa.loja.component.exceptions.DataConflictException;
import br.com.empresa.loja.component.exceptions.DataException;
import br.com.empresa.loja.component.exceptions.DataNotFoundException;
import br.com.empresa.loja.model.Autorizacao;
import br.com.empresa.loja.model.Funcionario;
import br.com.empresa.loja.model.Usuario;
import br.com.empresa.loja.repo.AutorizacaoRepo;
import br.com.empresa.loja.repo.FuncionarioRepo;
import br.com.empresa.loja.repo.UsuarioRepo;

@Service
public class FuncionarioService {
	@Autowired
	private FuncionarioRepo funcionarioRepo;
	@Autowired
	private AutorizacaoRepo authRepo;
	@Autowired
	private UsuarioRepo userRepo;
	
	public Page<Funcionario> listar(String nome, Integer pageNo, Integer pageSize, String sortBy) {
		Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		
		if(!nome.isEmpty()) {
			return funcionarioRepo.findByNomeContainingIgnoreCaseAndDesativadoFalse(nome, paging);
		} else {
			return funcionarioRepo.findByDesativadoFalse(paging);
		}
	}

	public long totalFuncionarios() {
		return funcionarioRepo.countByDesativadoFalse();
	}
	
	public Funcionario salvar(Funcionario obj) throws DataException {
		if(obj.getIdFuncionario() != null) {
			Funcionario funcionario = funcionarioRepo.findByIdFuncionarioAndDesativadoFalse(obj.getIdFuncionario()).orElse(null);
			
			if(funcionario != null) {
				funcionario.setNome(obj.getNome());

				var conta = funcionario.getConta();
				if(conta == null) conta = new Usuario();
				
				var outraConta = obj.getConta();
				conta.setEmail(outraConta.getEmail());
				conta.setHabilitado(outraConta.isHabilitado());
				
				var senha = outraConta.getSenha();
				if(senha != null && !senha.isEmpty()) {
					conta.setPassword(senha);
				}
				
				var auth = conta.getAutorizacao();
				if(auth == null) auth = new Autorizacao();
				
				var outroAuth = outraConta.getAutorizacao();
				if(outroAuth != null) {
					auth.setEmail(outroAuth.getEmail());
					auth.setPerfil(outroAuth.getPerfil());
				}
				
				conta.setAutorizacao(auth);
				funcionario.setConta(conta);
				
				authRepo.save(auth);
				userRepo.save(conta);

				return funcionarioRepo.save(funcionario);
			} else {
				throw new DataNotFoundException("O Funcionário não foi encontrado");
			}
		} else {
			try {
				authRepo.save(obj.getConta().getAutorizacao());
				obj.getConta().setPassword(obj.getConta().getSenha());
				userRepo.save(obj.getConta());

				return funcionarioRepo.save(obj);
			} catch(DataIntegrityViolationException ex) {
				throw new DataConflictException("Nome de Funcionário duplicado");
			}
		}
	}
	
	public boolean remover(long id) {
		Optional<Funcionario> obj = funcionarioRepo.findById(id);
		
		if(obj.isPresent()) {
			Funcionario oObj = obj.get();
			funcionarioRepo.delete(oObj);
			return true;
		}
		return false;
	}
	
	public Optional<Funcionario> localizar(long id) {
		return funcionarioRepo.findById(id);
	}
	
	public Optional<Funcionario> localizar(Usuario obj) {
		return funcionarioRepo.findByConta(obj);
	}
}
