package br.com.empresa.loja.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.empresa.loja.component.exceptions.DataException;
import br.com.empresa.loja.model.Alternativa;
import br.com.empresa.loja.repo.AlternativaRepo;

@Service
public class AlternativaService {
	@Autowired
	private AlternativaRepo alternativaRepo;
	
	public boolean remover(long id) throws DataException {
		Optional<Alternativa>  aAlternativa = localizar(id);
		if(aAlternativa.isPresent()) {
			Alternativa alternativa = aAlternativa.get();
			alternativaRepo.delete(alternativa);
			return true;
		}
		
		return false;
	}
	
	public Optional<Alternativa> localizar(long id) {
		return alternativaRepo.findById(id);
	}
}