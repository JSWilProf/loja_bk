package br.com.empresa.loja.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.empresa.loja.component.exceptions.DataConflictException;
import br.com.empresa.loja.component.exceptions.DataException;
import br.com.empresa.loja.component.exceptions.DataNotFoundException;
import br.com.empresa.loja.model.Produto;
import br.com.empresa.loja.repo.ProdutoRepo;

@Service
public class ProdutoService {
	@Autowired
	private ProdutoRepo produtoRepo;
	
	public Page<Produto> listar(Integer pageNo, Integer pageSize, String sortBy) {
		Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		
		return produtoRepo.findByDesativadoFalse(paging);
	}
	
	public Produto salvar(Produto obj) throws DataException {
		try {
			if(obj.getIdProduto() != null) {
				Produto novoObj = produtoRepo.findByIdProdutoAndDesativadoFalse(obj.getIdProduto()).orElse(null);
						
				novoObj.setNome(obj.getNome());
				novoObj.setDescricao(obj.getDescricao());
				novoObj.setPrecoUnitario(obj.getPrecoUnitario());
				novoObj.setDesativado(obj.isDesativado());
				
				return produtoRepo.save(novoObj);
			} else {
				return produtoRepo.save(obj);
			}
		} catch(Exception ex) {
			System.out.println(ex.getMessage());
			throw new DataConflictException("Produto duplicado");
		}
	}	

	public Produto salvarDados(Produto obj) throws DataException {
		Optional<Produto> oObj = produtoRepo.findById(obj.getIdProduto());
		
		if(oObj.isPresent()) {
			Produto novoObj = oObj.get();
			
			novoObj.setFoto(obj.getFoto());
			novoObj.setMimeType(obj.getMimeType());
				
			return produtoRepo.save(novoObj);
		} else {
			throw new DataNotFoundException("O Produto não foi encontrado");
		}
	}
		
	public boolean remover(long id) {
		Optional<Produto> obj = produtoRepo.findById(id);
		
		if(obj.isPresent()) {
			Produto oObj = obj.get();
			oObj.setDesativado(true);
			produtoRepo.save(oObj);	
			return true;
		}
		
		return false;
	}
	
	public Optional<Produto> localizar(long id) {
		return produtoRepo.findByIdProdutoAndDesativadoFalse(id);
	}
}
