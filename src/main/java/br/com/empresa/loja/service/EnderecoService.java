package br.com.empresa.loja.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import br.com.empresa.loja.component.exceptions.DataConflictException;
import br.com.empresa.loja.component.exceptions.DataException;
import br.com.empresa.loja.component.exceptions.DataNotFoundException;
import br.com.empresa.loja.model.Cliente;
import br.com.empresa.loja.model.Endereco;
import br.com.empresa.loja.repo.EnderecoRepo;

@Service
public class EnderecoService {
	@Autowired
	private EnderecoRepo enderecoRepo;
	@Autowired
	private ClienteService clienteService;
	
	public List<Endereco> listar(long idCliente) throws DataException {
		Optional<Cliente> cliente = clienteService.localizar(idCliente);
		if(!cliente.isPresent()) {
			throw new DataNotFoundException("Cliente não encontrado");
		} else {
			return enderecoRepo.findByCliente(cliente.get());
		}
	}

	public Endereco salvar(Endereco obj, long idCliente) throws DataException {
		if(obj.getIdEndereco() != null) {
			Endereco endereco = enderecoRepo.findById(obj.getIdEndereco()).orElse(null);
			if(endereco != null) {
				endereco.setNome(obj.getNome());
				endereco.setLogradouro(obj.getLogradouro());
				endereco.setNumero(obj.getNumero());
				endereco.setCep(obj.getCep());
				endereco.setComplemento(obj.getComplemento());
				endereco.setCidade(obj.getCidade());
				endereco.setEstado(obj.getEstado());

				return enderecoRepo.save(endereco);
			} else {
				throw new DataNotFoundException("O Endereço não foi encontrado");
			}
		} else {
			try {
				Optional<Cliente> oCliente = clienteService.localizar(idCliente);
				if(oCliente.isPresent()) {
					Cliente cliente = oCliente.get();
					obj.setCliente(cliente);
					
					List<Endereco> lista = cliente.getEndereco();
					if(lista == null) lista = new ArrayList<>();
					if(!lista.contains(obj)) {
						Endereco endereco =  enderecoRepo.save(obj);
						lista.add(endereco);
						cliente.setEndereco(lista);
						clienteService.salvar(cliente);

						return endereco;
					} else {
						throw new DataConflictException("Endereço duplicado");
					}
				} else {
					throw new DataNotFoundException("Cliente não encontrado");
				}
			} catch(DataIntegrityViolationException ex) {
				throw new DataConflictException("Endereço duplicado");
			}
		}
	}
	
	public boolean remover(long id) throws DataException {
		Optional<Endereco> obj = enderecoRepo.findById(id);
		
		if(obj.isPresent()) {
			Endereco oObj = obj.get();
			Cliente cliente = oObj.getCliente();
			cliente.getEndereco().removeIf(e -> e.equals(oObj));
			clienteService.salvar(cliente);
			enderecoRepo.delete(oObj);
			return true;
		}
		return false;
	}
	
	public Optional<Endereco> localizar(long id) {
		return enderecoRepo.findById(id);
	}
}
