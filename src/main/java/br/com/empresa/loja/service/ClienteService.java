package br.com.empresa.loja.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.empresa.loja.component.exceptions.DataConflictException;
import br.com.empresa.loja.component.exceptions.DataException;
import br.com.empresa.loja.component.exceptions.DataNotFoundException;
import br.com.empresa.loja.model.Autorizacao;
import br.com.empresa.loja.model.Cliente;
import br.com.empresa.loja.model.Usuario;
import br.com.empresa.loja.repo.AutorizacaoRepo;
import br.com.empresa.loja.repo.ClienteRepo;
import br.com.empresa.loja.repo.UsuarioRepo;

@Service
public class ClienteService {
	@Autowired
	private ClienteRepo clienteRepo;
	@Autowired
	private AutorizacaoRepo authRepo;
	@Autowired
	private UsuarioRepo userRepo;
	
	public Page<Cliente> listar(String nome, Integer pageNo, Integer pageSize, String sortBy) {
		Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		
		if(!nome.isEmpty()) {
			return clienteRepo.findByNomeContainingIgnoreCaseAndDesativadoFalse(nome, paging);
		} else {
			return clienteRepo.findByDesativadoFalse(paging);
		}
	}

	public long totalClientes() {
		return clienteRepo.countByDesativadoFalse();
	}
	
	public Cliente salvar(Cliente obj) throws DataException {
		if(obj.getIdCliente() != null) {
			Cliente cliente = clienteRepo.findByIdClienteAndDesativadoFalse(obj.getIdCliente()).orElse(null);
					
			if(cliente != null) {
				cliente.setNome(obj.getNome());
				cliente.setCpf(obj.getCpf());
				cliente.setTelefone(obj.getTelefone());				
				
				var conta = cliente.getConta();
				if(conta == null) conta = new Usuario();
				
				var outraConta = obj.getConta();
				conta.setEmail(outraConta.getEmail());
				conta.setHabilitado(outraConta.isHabilitado());
				
				var senha = outraConta.getSenha();
				if(senha != null && !senha.isEmpty()) {
					conta.setPassword(senha);
				}
				
				var auth = conta.getAutorizacao();
				if(auth == null) auth = new Autorizacao();
				
				var outroAuth = outraConta.getAutorizacao();
				if(outroAuth != null) {
					auth.setEmail(outroAuth.getEmail());
					auth.setPerfil(outroAuth.getPerfil());
				}
				
				conta.setAutorizacao(auth);
				cliente.setConta(conta);
				
				authRepo.save(auth);
				userRepo.save(conta);

				return clienteRepo.save(cliente);
			} else {
				throw new DataNotFoundException("Cliente não foi encontado");
			}
		} else {
			try {
				authRepo.save(obj.getConta().getAutorizacao());
				obj.getConta().setPassword(obj.getConta().getSenha());
				userRepo.save(obj.getConta());

				return clienteRepo.save(obj);
			} catch(DataIntegrityViolationException ex) {
				throw new DataConflictException("Nome de Cliente duplicado");
			}
		}
	}
	
	public boolean remover(long id) {
		Optional<Cliente> obj = clienteRepo.findById(id);
		
		if(obj.isPresent()) {
			Cliente oObj = obj.get();
			Usuario conta = oObj.getConta();
			conta.setHabilitado(false);
			userRepo.save(conta);
			oObj.setDesativado(true);
			clienteRepo.save(oObj);	
			return true;
		}		
		return false;
	}
	
	public Optional<Cliente> localizar(long id) {
		return clienteRepo.findById(id);
	}
	
	public Optional<Cliente> localizar(Usuario obj) {
		return clienteRepo.findByConta(obj);
	}
}
