package br.com.empresa.loja.component;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import br.com.empresa.loja.model.Usuario;

@Component
public class SecurityFacade  {
	public String getUserName() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth != null) {
			Usuario user = (Usuario)auth.getPrincipal();
			return user.getEmail();
		}
		return null;
	}
}
