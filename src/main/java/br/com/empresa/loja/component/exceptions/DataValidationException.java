package br.com.empresa.loja.component.exceptions;

import org.springframework.validation.BindingResult;

import lombok.Getter;


public class DataValidationException extends DataException {
	private static final long serialVersionUID = 1L;
	@Getter
	private BindingResult result;
	
	public DataValidationException(String message, BindingResult result) {
		super(message);
		this.result = result;
	}
	
	public DataValidationException(String message, Throwable t, BindingResult result) {
		super(message, t);
		this.result = result;
	}
}