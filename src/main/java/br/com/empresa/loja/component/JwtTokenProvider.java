package br.com.empresa.loja.component;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import br.com.empresa.loja.model.valueObject.JwtAuthenticationResponse;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JwtTokenProvider {
   private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

   @Value("${app.jwtSecret}")
   private String jwtSecret;

   @Value("${app.jwtExpirationInSec}")
   private int jwtExpirationInMs;

   public JwtAuthenticationResponse generateToken(Authentication authentication) throws Exception {
      return generateToken(authentication, null, null);
   }

   public JwtAuthenticationResponse generateToken(Authentication authentication, Long id, String perfil)
         throws Exception {
      try {
         User userPrincipal = (User) authentication.getPrincipal();

         LocalDateTime now = LocalDateTime.now(Clock.systemUTC());
         LocalDateTime expiryDate = now.plus(jwtExpirationInMs, ChronoUnit.SECONDS);

         String token = Jwts.builder().setSubject(userPrincipal.getUsername()).setIssuedAt(DateUtils.asDate(now))
               .setExpiration(DateUtils.asDate(expiryDate)).signWith(SignatureAlgorithm.HS512, jwtSecret).compact();

         return id == null ? new JwtAuthenticationResponse(token, expiryDate)
               : new JwtAuthenticationResponse(token, expiryDate, perfil, id);
      } catch (Exception ex) {
         throw new Exception("Falha na autenticação");
      }
   }

   public String getUserIdFromJWT(String token) {
      return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
   }

   public boolean validateToken(String authToken) {
      try {
         Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
         return true;
      } catch (MalformedJwtException ex) {
         logger.error("Invalid JWT token");
      } catch (ExpiredJwtException ex) {
         logger.error("Expired JWT token");
      } catch (UnsupportedJwtException ex) {
         logger.error("Unsupported JWT token");
      } catch (IllegalArgumentException ex) {
         logger.error("JWT claims string is empty.");
      }
      return false;
   }
}