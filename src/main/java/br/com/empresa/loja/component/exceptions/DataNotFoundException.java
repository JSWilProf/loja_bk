package br.com.empresa.loja.component.exceptions;

public class DataNotFoundException extends DataException {
	private static final long serialVersionUID = 1L;
	
	public DataNotFoundException(String message) {
		super(message);
	}
	
	public DataNotFoundException(String message, Throwable t) {
		super(message, t);
	}
}