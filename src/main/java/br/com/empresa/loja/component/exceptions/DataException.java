package br.com.empresa.loja.component.exceptions;


public abstract class DataException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public DataException(String message) {
		super(message);
	}
	
	public DataException(String message, Throwable t) {
		super(message, t);
	}
}
