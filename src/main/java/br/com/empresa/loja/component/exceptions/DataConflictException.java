package br.com.empresa.loja.component.exceptions;

public class DataConflictException extends DataException {
	private static final long serialVersionUID = 1L;
	
	public DataConflictException(String message) {
		super(message);
	}
	
	public DataConflictException(String message, Throwable t) {
		super(message, t);
	}
}