package br.com.empresa.loja.repo;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.empresa.loja.model.Produto;


public interface ProdutoRepo extends JpaRepository<Produto, Long> {
	Optional<Produto> findByIdProdutoAndDesativadoFalse(Long id);
	Optional<Produto> findByNomeAndDesativadoFalse(String nome);
	Page<Produto> findByDesativadoFalse(Pageable paging);
	Page<Produto> findByNomeContainingIgnoreCaseAndDesativadoFalse(String nome, Pageable paging);
	long countByDesativadoFalse();
}