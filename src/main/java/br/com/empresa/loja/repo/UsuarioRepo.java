package br.com.empresa.loja.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.empresa.loja.model.Usuario;


public interface UsuarioRepo extends JpaRepository<Usuario, Long> {
	Optional<Usuario> findByEmail(String email);
}
