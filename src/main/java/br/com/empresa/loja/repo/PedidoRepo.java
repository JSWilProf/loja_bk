package br.com.empresa.loja.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.empresa.loja.model.Cliente;
import br.com.empresa.loja.model.Pedido;

public interface PedidoRepo extends JpaRepository<Pedido, Long> {
	List<Pedido> findByCliente(Cliente cliente);
}
