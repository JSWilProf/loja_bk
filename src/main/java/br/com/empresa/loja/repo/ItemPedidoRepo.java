package br.com.empresa.loja.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.empresa.loja.model.ItemPedido;

public interface ItemPedidoRepo extends JpaRepository<ItemPedido, Long> {
}
