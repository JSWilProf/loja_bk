package br.com.empresa.loja.repo;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.empresa.loja.model.Cliente;
import br.com.empresa.loja.model.Usuario;

public interface ClienteRepo extends JpaRepository<Cliente, Long> {
	Optional<Cliente> findByIdClienteAndDesativadoFalse(Long id);
	Optional<Cliente> findByNomeAndDesativadoFalse(String nome);
	Optional<Cliente> findByConta(Usuario usuario);
	Page<Cliente> findByDesativadoFalse(Pageable paging);
	Page<Cliente> findByNomeContainingIgnoreCaseAndDesativadoFalse(String nome, Pageable paging);
	long countByDesativadoFalse();
}
