package br.com.empresa.loja.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.empresa.loja.model.Produto;
import br.com.empresa.loja.model.Questao;

public interface QuestaoRepo extends JpaRepository<Questao, Long> {
	List<Questao> findByProduto(Produto produto);
}
