package br.com.empresa.loja.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.empresa.loja.model.Cliente;
import br.com.empresa.loja.model.Endereco;

public interface EnderecoRepo extends JpaRepository<Endereco, Long> {
	List<Endereco> findByCliente(Cliente cliente);
}
