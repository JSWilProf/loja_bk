package br.com.empresa.loja.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.empresa.loja.model.Resposta;

public interface RespostaRepo extends JpaRepository<Resposta, Long>{
}
