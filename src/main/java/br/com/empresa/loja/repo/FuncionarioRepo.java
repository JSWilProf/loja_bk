package br.com.empresa.loja.repo;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.empresa.loja.model.Funcionario;
import br.com.empresa.loja.model.Usuario;

public interface FuncionarioRepo extends JpaRepository<Funcionario, Long> {
	Optional<Funcionario> findByIdFuncionarioAndDesativadoFalse(Long id);
	Optional<Funcionario> findByNomeAndDesativadoFalse(String nome);
	Optional<Funcionario> findByConta(Usuario usuario);
	Page<Funcionario> findByDesativadoFalse(Pageable paging);
	Page<Funcionario> findByNomeContainingIgnoreCaseAndDesativadoFalse(String nome, Pageable paging);
	long countByDesativadoFalse();
}
