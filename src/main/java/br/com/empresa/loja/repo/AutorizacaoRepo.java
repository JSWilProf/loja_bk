package br.com.empresa.loja.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.empresa.loja.model.Autorizacao;

public interface AutorizacaoRepo extends JpaRepository<Autorizacao, String>{
}
