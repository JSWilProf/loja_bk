package br.com.empresa.loja.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.empresa.loja.model.Alternativa;

public interface AlternativaRepo extends JpaRepository<Alternativa, Long> {
}
