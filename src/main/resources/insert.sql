insert into loja.dbo.autorizacao (email, perfil)
values ('admin@domain.com', 'ROLE_ADMIN');

SET IDENTITY_INSERT loja.dbo.usuario ON;
insert into loja.dbo.usuario (idUsuario, email, senha, habilitado, autorizacao_idAutorizacao)
values (1, 'admin@domain.com', '$2a$10$j0fT8GIYZSAbO7UFLLTrEuTa4PTWvyDrWt9w3mWfTwrrpUdMcNG3y', 1, 1);
SET IDENTITY_INSERT loja.dbo.usuario OFF;

insert into loja.dbo.funcionario (desativado, nome, conta_idUsuario)
values (0, 'admin', 1);